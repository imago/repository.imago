# Imago repository for Kodi

This repository contains the ImagoTV add-on and its dependencies.  
After installing it in Kodi, it will allow you to receive automatic updates for
all its add-ons.

## How to install

- Download the archive named `repository.imago-<version>.zip` in the latest
  version available in the
  [releases](https://framagit.org/imago/repository.imago/-/releases) page
- Use the [Install from a ZIP file](https://kodi.wiki/view/Add-on_manager#How_to_install_from_a_ZIP_file) feature in Kodi with this file

## Content

Add-ons in this repository:
- [ImagoTV](https://framagit.org/imago/plugin.video.imagotv)
- Imago Repository
- [PeerTube](https://framagit.org/StCyr/plugin.video.peertube)
- [script.module.libtorrent](https://github.com/thombet/script.module.libtorrent)

## Release

To release a new version of this repository:
- update the file `config.yaml`
- clone `https://framagit.org/thombet/scripts-for-kodi-add-ons`
- execute the script `update-repository/update-repository.py` from the root of
  this repository: `python3 scripts-for-kodi-add-ons/update-repository/update-repository.py`
- commit and push the changes
- if a new version of the repository was created, create a new release in GitLab
  with the release notes from the "news" tag in `repository.imago/addon.xml`.
  Also add a link with the following values:
  - URL: URL of the file `repository.imago/repository.imago-<version>.zip`
  - Link title: repository.imago-\<version\>.zip